# MessagePass

This is a repository of containers who pass a given message around.

Containers using the following technologies can be brought up using `podman-componse` or `docker-componse`:
* [httpbin](https://httpbin.org/)
* [tornado](https://www.tornadoweb.org/en/stable/)

## Getting started

1. Install podman
2. Install podman-compose
3. `podman-compose up -d`
4. `curl localhost:<container-port>/send -d '<message body>'`

## Port directory

`docker-compose.yaml` is the source of truth for 